module gitlab.com/imaginadio/golang/bank-broker-app/app-polling

go 1.15

require (
	github.com/joho/godotenv v1.3.0 // indirect
	github.com/spf13/viper v1.7.1
	github.com/streadway/amqp v1.0.0
	go.uber.org/zap v1.16.0
)
