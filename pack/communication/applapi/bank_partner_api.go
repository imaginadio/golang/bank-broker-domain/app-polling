package applapi

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"

	apppolling "gitlab.com/imaginadio/golang/bank-broker-app/app-polling"
	"go.uber.org/zap"
)

const url_api_app = "/api/jobs"

type BankPartnerApi struct {
	config BankPartnerApiConfig
	client *http.Client
}

type BankPartnerApiConfig struct {
	Url  string
	Port string
}

func NewBankPartnerApi(config BankPartnerApiConfig, client *http.Client) *BankPartnerApi {
	return &BankPartnerApi{
		config: config,
		client: client,
	}
}

func (c *BankPartnerApi) RequestApplicationStatus(id string) (apppolling.ApplicationJobResponse, error) {
	var result apppolling.ApplicationJobResponse
	url := fmt.Sprintf("%s:%s%s?application_id=%s", c.config.Url, c.config.Port, url_api_app, id)
	resp, err := http.Get(url)
	if err != nil {
		return result, err
	}

	respBody, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return result, err
	}
	zap.L().Info("Bank partner api return result", zap.String("url", url), zap.ByteString("response", respBody))

	return result, json.Unmarshal(respBody, &result)
}
