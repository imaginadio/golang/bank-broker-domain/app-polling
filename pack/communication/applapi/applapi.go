package applapi

import (
	"net/http"

	apppolling "gitlab.com/imaginadio/golang/bank-broker-app/app-polling"
)

type Application interface {
	RequestApplicationStatus(id string) (apppolling.ApplicationJobResponse, error)
}

type ApplicationApi struct {
	Application
}

func NewApplicationApi(config BankPartnerApiConfig, client *http.Client) *ApplicationApi {
	return &ApplicationApi{
		Application: NewBankPartnerApi(config, client),
	}
}
