package applqueue

import (
	"github.com/streadway/amqp"
	apppolling "gitlab.com/imaginadio/golang/bank-broker-app/app-polling"
)

type Application interface {
	SendReadyToQueue(app apppolling.ApplicationJobResponse) error
}

type ApplicationQueue struct {
	Application
}

func NewApplicationQueue(ch *amqp.Channel) *ApplicationQueue {
	return &ApplicationQueue{
		Application: NewRabbitQueue(ch),
	}
}
