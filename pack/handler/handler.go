package handler

import "gitlab.com/imaginadio/golang/bank-broker-app/app-polling/pack/service"

type Handler struct {
	services *service.Service
}

func NewHandler(services *service.Service) *Handler {
	return &Handler{services: services}
}
