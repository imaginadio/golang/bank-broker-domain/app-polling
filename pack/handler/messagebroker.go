package handler

import (
	"time"

	"github.com/spf13/viper"
	"github.com/streadway/amqp"
	"gitlab.com/imaginadio/golang/bank-broker-app/app-polling/pack/communication/applqueue"
	"go.uber.org/zap"
)

const (
	consumer_name = "ApplicationPollingServer"
)

func (h *Handler) processAmqpMessage(d amqp.Delivery) {
	application_id := string(d.Body)

	appResponse, err := h.services.Application.RequestApplicationStatus(application_id)
	if err != nil {
		zap.L().Error("Error on RequestApplicationStatus", zap.String("msg", err.Error()))
		d.Nack(false, true)
		return
	}

	if appResponse.Status != "pending" {
		// send message to rabbitmq if status is final
		if err := h.services.Application.SendReadyToQueue(appResponse); err != nil {
			zap.L().Error("Error on SendReadyToQueue", zap.String("msg", err.Error()))
			d.Nack(false, true)
			return
		}
	} else {
		// if status not final sleep 1 sec to prevent spamming bank partner api
		time.Sleep(1 * time.Second)
		d.Nack(false, true)
		return
	}

	d.Ack(false)
}

func (h *Handler) InitMessageQueueConsumer(ch *amqp.Channel) {
	if err := ch.Qos(viper.GetInt("mbroker.qos.prefetch_count"), viper.GetInt("mbroker.qos.prefetch_size"), false); err != nil {
		zap.S().Fatalf("Error on setting Qos: %s", err.Error())
	}

	pendingApps, err := ch.Consume(
		applqueue.Pending_applications_queue,
		consumer_name,
		false,
		false,
		false,
		false,
		nil,
	)
	if err != nil {
		zap.S().Fatalf("Error on consume to queue %s: %s", applqueue.Pending_applications_queue, err.Error())
	}

	go func() {
		for d := range pendingApps {
			go h.processAmqpMessage(d)
		}
	}()
}
