package service

import (
	apppolling "gitlab.com/imaginadio/golang/bank-broker-app/app-polling"
	"gitlab.com/imaginadio/golang/bank-broker-app/app-polling/pack/communication/applapi"
	"gitlab.com/imaginadio/golang/bank-broker-app/app-polling/pack/communication/applqueue"
)

type Application interface {
	RequestApplicationStatus(id string) (apppolling.ApplicationJobResponse, error)
	SendReadyToQueue(app apppolling.ApplicationJobResponse) error
}

type Service struct {
	Application
}

func NewService(api *applapi.ApplicationApi, queue *applqueue.ApplicationQueue) *Service {
	return &Service{
		Application: NewApplicationService(api.Application, queue.Application),
	}
}
