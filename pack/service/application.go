package service

import (
	apppolling "gitlab.com/imaginadio/golang/bank-broker-app/app-polling"
	"gitlab.com/imaginadio/golang/bank-broker-app/app-polling/pack/communication/applapi"
	"gitlab.com/imaginadio/golang/bank-broker-app/app-polling/pack/communication/applqueue"
)

type ApplicationService struct {
	api   applapi.Application
	queue applqueue.Application
}

func NewApplicationService(api applapi.Application, queue applqueue.Application) *ApplicationService {
	return &ApplicationService{
		api:   api,
		queue: queue,
	}
}

func (s *ApplicationService) RequestApplicationStatus(id string) (apppolling.ApplicationJobResponse, error) {
	// send http request to bank partner
	return s.api.RequestApplicationStatus(id)
}

func (s *ApplicationService) SendReadyToQueue(app apppolling.ApplicationJobResponse) error {
	// send message to broker when application has final status
	return s.queue.SendReadyToQueue(app)
}
