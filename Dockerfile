FROM golang:1.15-alpine as build-stage
WORKDIR /app/bank-polling-app
COPY go.mod .
COPY go.sum .
RUN go mod download
COPY . .
RUN go build -o ./out/bank-polling-app ./cmd

FROM alpine:3.12.3 as production-stage

WORKDIR /app

COPY --from=build-stage /app/bank-polling-app/out/bank-polling-app .
COPY configs ./configs
ENTRYPOINT [ "/app/bank-polling-app" ]
