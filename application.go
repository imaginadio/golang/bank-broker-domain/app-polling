package apppolling

type ApplicationJobResponse struct {
	Id            string `json:"id"`
	ApplicationId string `json:"application_id" db:"application_id"`
	Status        string `json:"status" db:"status"`
}
