package main

import (
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"

	"github.com/joho/godotenv"
	"github.com/spf13/viper"
	"gitlab.com/imaginadio/golang/bank-broker-app/app-polling/pack/communication/applapi"
	"gitlab.com/imaginadio/golang/bank-broker-app/app-polling/pack/communication/applqueue"
	"gitlab.com/imaginadio/golang/bank-broker-app/app-polling/pack/handler"
	"gitlab.com/imaginadio/golang/bank-broker-app/app-polling/pack/service"
	"go.uber.org/zap"
)

func main() {
	// init logger
	logger := initLogger()
	defer logger.Sync()

	undo := zap.ReplaceGlobals(logger)
	defer undo()

	// init configs
	if err := initConfig(); err != nil {
		zap.L().Fatal("Error while reading config file", zap.String("msg", err.Error()))
	}

	// init message broker
	queueConnection, err := applqueue.NewConnection(applqueue.Config{
		Host:     viper.GetString("mbroker.host"),
		Port:     viper.GetString("mbroker.port"),
		User:     viper.GetString("mbroker.user"),
		Password: os.Getenv("RABBIT_PASSWORD"),
	})
	if err != nil {
		zap.L().Fatal("Error while connecting to message broker", zap.String("msg", err.Error()))
	}
	defer queueConnection.Close()

	amqpChannel, err := queueConnection.Channel()
	if err != nil {
		zap.L().Fatal("Error while creating broker channel", zap.String("msg", err.Error()))
	}
	defer amqpChannel.Close()

	applqueue.DeclareQueues(amqpChannel)

	api := applapi.NewApplicationApi(
		applapi.BankPartnerApiConfig{
			Url:  viper.GetString("bankapi.url"),
			Port: viper.GetString("bankapi.port"),
		},
		&http.Client{},
	)
	queue := applqueue.NewApplicationQueue(amqpChannel)
	services := service.NewService(api, queue)
	handlers := handler.NewHandler(services)

	// init incoming message broker queue
	handlers.InitMessageQueueConsumer(amqpChannel)

	zap.L().Info("App started")

	quit := make(chan os.Signal)
	signal.Notify(quit, syscall.SIGTERM, syscall.SIGINT)
	<-quit

	zap.L().Info("App shutting down")
}

func initConfig() error {
	godotenv.Load()
	viper.AddConfigPath("configs")
	viper.SetConfigName("config")
	return viper.ReadInConfig()
}

func initLogger() *zap.Logger {
	logger, err := zap.NewProduction()
	if err != nil {
		log.Fatalf("Error while init logger: %s", err.Error())
	}
	return logger
}
